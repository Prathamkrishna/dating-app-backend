package com.spotifydatingapp.SpotifyDatingApp.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spotifydatingapp.SpotifyDatingApp.models.AuthenticateModel;
import com.spotifydatingapp.SpotifyDatingApp.services.interfaces.SpotifyJwtCommonService;
import net.minidev.json.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpotifyToken {

    private final SpotifyJwtCommonService spotifyJwtCommonService;

    public SpotifyToken(SpotifyJwtCommonService spotifyJwtCommonService) {
        this.spotifyJwtCommonService = spotifyJwtCommonService;
    }

    @PostMapping("/authenticate")
    public JSONObject getUserSpotifyDetails(@RequestBody AuthenticateModel authenticateModel) throws JsonProcessingException {
        return spotifyJwtCommonService.getJwtAndResult(authenticateModel.getToken());
    }

}
