package com.spotifydatingapp.SpotifyDatingApp.controllers;

import com.spotifydatingapp.SpotifyDatingApp.models.RegisteredUserDetailsModel;
import com.spotifydatingapp.SpotifyDatingApp.services.interfaces.AddUserImageAndDetailsService;
import com.spotifydatingapp.SpotifyDatingApp.utils.FileSizeConversion;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

@RestController
@RequestMapping("/upload")
public class ImageAndDetailsUpload {

    private final AddUserImageAndDetailsService addUserImageAndDetailsService;

    public ImageAndDetailsUpload(AddUserImageAndDetailsService addUserImageAndDetailsService) {
        this.addUserImageAndDetailsService = addUserImageAndDetailsService;
    }

    @PostMapping(value = "/image")
    public ResponseEntity<JSONObject> uploadImageDetails(@RequestParam("image")MultipartFile file, RedirectAttributes redirectAttributes) {
        if (FileSizeConversion.changeBytesToMegabytes(file.getSize()) > 15){
            return new ResponseEntity<>(new JSONObject(), HttpStatus.PAYLOAD_TOO_LARGE);
        }
        return new ResponseEntity<>(addUserImageAndDetailsService.userImageManipulation(file), HttpStatus.OK);
    }

    @PostMapping( "/details")
    public JSONObject uploadUserEnteredDetails(@RequestBody RegisteredUserDetailsModel registeredUserDetailsModel){
        return addUserImageAndDetailsService.registeredUserDetailsManipulation(registeredUserDetailsModel);
    }
}
