package com.spotifydatingapp.SpotifyDatingApp.security;

import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.UserDetailsServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    private final UserDetailsServiceRepository userDetailsServiceRepository;

    @Autowired
    public MyUserDetailsService(UserDetailsServiceRepository userDetailsServiceRepository) {
        this.userDetailsServiceRepository = userDetailsServiceRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userDetailsServiceRepository.loadByUserInfo(email);
    }
}
