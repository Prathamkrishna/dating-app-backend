package com.spotifydatingapp.SpotifyDatingApp.utils;

import org.springframework.stereotype.Service;

@Service
public class FileSizeConversion {

    private static int byteToMegabyte = 1024*1024;

    public static int changeBytesToMegabytes(long sizeInBytes){
        return (int) (sizeInBytes / byteToMegabyte);
    }

}
