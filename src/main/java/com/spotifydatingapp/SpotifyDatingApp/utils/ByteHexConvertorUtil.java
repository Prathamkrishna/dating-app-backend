package com.spotifydatingapp.SpotifyDatingApp.utils;

public class ByteHexConvertorUtil {

    private static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes){
        char[] chars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; i++){
            int v = bytes[i] & 0xFF;
            chars[i * 2] = hexArray[v >>> 4];
            chars[i * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(chars);
    }

}
