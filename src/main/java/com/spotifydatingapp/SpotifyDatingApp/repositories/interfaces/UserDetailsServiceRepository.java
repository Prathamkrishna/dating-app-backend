package com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces;

import org.springframework.security.core.userdetails.User;

public interface UserDetailsServiceRepository {
    User loadByUserInfo(String email);
}
