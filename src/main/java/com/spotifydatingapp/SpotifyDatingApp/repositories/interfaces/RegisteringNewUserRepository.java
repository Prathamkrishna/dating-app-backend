package com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces;

import com.spotifydatingapp.SpotifyDatingApp.models.RegisteredUserDetailsModel;

public interface RegisteringNewUserRepository {
    Boolean addUserImagePath(String path, String name, int imageSize);
    Boolean addRegisteredUserDetails(RegisteredUserDetailsModel registeredUserDetailsModel);
}
