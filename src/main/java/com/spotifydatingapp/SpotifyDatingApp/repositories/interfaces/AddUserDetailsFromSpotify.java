package com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces;

import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;

public interface AddUserDetailsFromSpotify {
    void addBasicUserInfo(SpotifyUserNameAndEmailModel spotifyUserNameAndEmailModel);
}
