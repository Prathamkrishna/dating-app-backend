package com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces;

import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;

public interface SearchIfUserExists {
    Boolean checkUserExistenceInRepository(SpotifyUserNameAndEmailModel spotifyUserNameAndEmailModel);
}
