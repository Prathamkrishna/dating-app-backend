package com.spotifydatingapp.SpotifyDatingApp.repositories.impl;

import com.spotifydatingapp.SpotifyDatingApp.models.RegisteredUserDetailsModel;
import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.RegisteringNewUserRepository;
import org.postgresql.util.PSQLException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class RegisteringNewUserRepositoryImpl implements RegisteringNewUserRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public RegisteringNewUserRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Boolean addUserImagePath(String path, String name, int imageSize) {
        String addUserImageDetails = "INSERT into userimageanddetails (email, imagepath, imagesize) values (:userEmail, :userImagePath, " + imageSize + ")";
        Map<String, String> addUserImageParams = new HashMap<>();
        addUserImageParams.put("userEmail", name);
        addUserImageParams.put("userImagePath", path);
        try {
            namedParameterJdbcTemplate.update(addUserImageDetails, addUserImageParams);
            return true;
        } catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean addRegisteredUserDetails(RegisteredUserDetailsModel registeredUserDetailsModel) {
        String registerNewUserSql = "UPDATE userImageAndDetails set age = " + registeredUserDetailsModel.getAge() + ", gender = :userGender, searchpreferencegender = :userSearchGender, userenteredname = :userName where email = :userEmail";
        Map<String, String> registerNewUserParams = new HashMap<>();
        registerNewUserParams.put("userEmail", registeredUserDetailsModel.getEmail());
        registerNewUserParams.put("userName", registeredUserDetailsModel.getName());
        registerNewUserParams.put("userGender", registeredUserDetailsModel.getSelfGender());
        registerNewUserParams.put("userSearchGender", registeredUserDetailsModel.getSearchPreferenceGender());
        try {
            namedParameterJdbcTemplate.update(registerNewUserSql, registerNewUserParams);
            return true;
        } catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }
}
