package com.spotifydatingapp.SpotifyDatingApp.repositories.impl;

import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;
import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.AddUserDetailsFromSpotify;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class AddUserDetailsFromSpotifyImpl implements AddUserDetailsFromSpotify {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public AddUserDetailsFromSpotifyImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public void addBasicUserInfo(SpotifyUserNameAndEmailModel spotifyUserNameAndEmailModel) {
        String addUserInfoStatement = "INSERT INTO USERINFO (email, username, roles) VALUES (:email, :username," + null + ")";
        Map<String, String> userInfoParams = new HashMap<>();
        userInfoParams.put("email", spotifyUserNameAndEmailModel.getEmail());
        userInfoParams.put("username", spotifyUserNameAndEmailModel.getDisplay_name());
        namedParameterJdbcTemplate.update(addUserInfoStatement, userInfoParams);
    }
}
