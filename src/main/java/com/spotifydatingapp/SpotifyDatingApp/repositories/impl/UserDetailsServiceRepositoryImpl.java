package com.spotifydatingapp.SpotifyDatingApp.repositories.impl;

import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.UserDetailsServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserDetailsServiceRepositoryImpl implements UserDetailsServiceRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public UserDetailsServiceRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public User loadByUserInfo(String email) {
        return getUserInfoFromDatabase(email);
    }

    protected User getUserInfoFromDatabase(String email){
        String searchJwtUser = "SELECT * FROM USERINFO WHERE email = :email";
        Map<String, String> searchJwtUserParams = new HashMap<>();
        searchJwtUserParams.put("email", email);
        System.out.println(email + "emaill");
        List<User> list = namedParameterJdbcTemplate.query(searchJwtUser, searchJwtUserParams, this::resultSetExtractor);
        System.out.println(list.get(0).getUsername());
        return list.get(0);
    }

    private User resultSetExtractor(ResultSet resultSet, int rowNum) throws SQLException {
        return new User(resultSet.getString(2), resultSet.getString(3), new ArrayList<>());
    }
}
