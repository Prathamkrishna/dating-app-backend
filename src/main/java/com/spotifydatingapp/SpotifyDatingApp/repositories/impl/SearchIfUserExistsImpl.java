package com.spotifydatingapp.SpotifyDatingApp.repositories.impl;

import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;
import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.SearchIfUserExists;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class SearchIfUserExistsImpl implements SearchIfUserExists {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public SearchIfUserExistsImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Boolean checkUserExistenceInRepository(SpotifyUserNameAndEmailModel spotifyUserNameAndEmailModel) {
        String searchSqlStatement = "SELECT COUNT(*) FROM USERINFO WHERE username = :username and email = :email";
        Map<String, String> searchParams = new HashMap<>();
        searchParams.put("username", spotifyUserNameAndEmailModel.getDisplay_name());
        searchParams.put("email", spotifyUserNameAndEmailModel.getEmail());
        Integer resultInt = namedParameterJdbcTemplate.queryForObject(searchSqlStatement, searchParams, Integer.class);
        return resultInt != 0;
    }
}
