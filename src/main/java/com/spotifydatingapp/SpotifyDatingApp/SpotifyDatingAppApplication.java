package com.spotifydatingapp.SpotifyDatingApp;

import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;
import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.SearchIfUserExists;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpotifyDatingAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotifyDatingAppApplication.class, args);
	}



}
