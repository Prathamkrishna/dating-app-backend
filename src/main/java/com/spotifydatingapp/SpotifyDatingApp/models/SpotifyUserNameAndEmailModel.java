package com.spotifydatingapp.SpotifyDatingApp.models;

import lombok.Data;

@Data
public class SpotifyUserNameAndEmailModel {
    private String email;
    private String display_name;
}
