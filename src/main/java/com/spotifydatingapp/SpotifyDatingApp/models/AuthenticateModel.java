package com.spotifydatingapp.SpotifyDatingApp.models;


public class AuthenticateModel {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
