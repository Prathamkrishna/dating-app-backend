package com.spotifydatingapp.SpotifyDatingApp.models;

import lombok.Data;

@Data
public class RegisteredUserDetailsModel {
    private String email;
    private String name;
    private int age;
    private String selfGender;
    private String searchPreferenceGender;
}
