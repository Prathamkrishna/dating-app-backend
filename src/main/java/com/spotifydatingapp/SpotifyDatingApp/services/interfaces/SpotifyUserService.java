package com.spotifydatingapp.SpotifyDatingApp.services.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;

public interface SpotifyUserService {
    SpotifyUserNameAndEmailModel getSpotifyUserData(String token) throws JsonProcessingException;
}
