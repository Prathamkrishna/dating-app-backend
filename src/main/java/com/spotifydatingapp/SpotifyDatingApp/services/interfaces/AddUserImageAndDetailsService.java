package com.spotifydatingapp.SpotifyDatingApp.services.interfaces;

import com.spotifydatingapp.SpotifyDatingApp.models.RegisteredUserDetailsModel;
import net.minidev.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

public interface AddUserImageAndDetailsService {
    JSONObject userImageManipulation(MultipartFile file);

    JSONObject registeredUserDetailsManipulation(RegisteredUserDetailsModel registeredUserDetailsModel);
}
