package com.spotifydatingapp.SpotifyDatingApp.services.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.minidev.json.JSONObject;

public interface SpotifyJwtCommonService {
    JSONObject getJwtAndResult(String token) throws JsonProcessingException;
}
