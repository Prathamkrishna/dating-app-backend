package com.spotifydatingapp.SpotifyDatingApp.services.impls;

import com.spotifydatingapp.SpotifyDatingApp.models.RegisteredUserDetailsModel;
import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.RegisteringNewUserRepository;
import com.spotifydatingapp.SpotifyDatingApp.services.interfaces.AddUserImageAndDetailsService;
import com.spotifydatingapp.SpotifyDatingApp.utils.FileSizeConversion;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class AddUserImageAndDetailsServiceImpl implements AddUserImageAndDetailsService {

    @Value("${imageFolderPath}")
    private String imageStoringPath;

    private final RegisteringNewUserRepository registeringNewUserRepository;

    public AddUserImageAndDetailsServiceImpl(RegisteringNewUserRepository registeringNewUserRepository) {
        this.registeringNewUserRepository = registeringNewUserRepository;
    }

    @Override
    public JSONObject userImageManipulation(MultipartFile file) {
        String filePath = imageStoringPath + "/" + file.getOriginalFilename() + ".jpeg";
        Boolean returnedType = null;
        try {
            if (checkIfExists(filePath)){
                returnedType = overwriteExistingFile(filePath, file.getBytes());
            }
            else returnedType = createNewFile(filePath, file.getBytes());
        } catch (Exception e){
            e.printStackTrace();
            returnedType = false;
        }
        if (returnedType) returnedType = registeringNewUserRepository.addUserImagePath(filePath, file.getOriginalFilename(), FileSizeConversion.changeBytesToMegabytes(file.getSize()));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("imageInsertion", returnedType);
        return jsonObject;
    }

    @Override
    public JSONObject registeredUserDetailsManipulation(RegisteredUserDetailsModel registeredUserDetailsModel) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("registerationSuccess", registeringNewUserRepository.addRegisteredUserDetails(registeredUserDetailsModel));
        return jsonObject;
    }

    protected Boolean checkIfExists(String filePath){
        File file = new File(filePath);
        return file.exists();
    }

    protected Boolean createNewFile(String filePath, byte[] bytes){
        File file = new File(filePath);
        System.out.println(filePath);
        try {
            if (file.createNewFile()){
                Files.write(Path.of(filePath), bytes);
                return true;
            } else return false;

        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    protected Boolean overwriteExistingFile(String filePath, byte[] bytes) {
        try {
            Files.write(Path.of(filePath), bytes);
            return true;
        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

}
