package com.spotifydatingapp.SpotifyDatingApp.services.impls;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;
import com.spotifydatingapp.SpotifyDatingApp.services.interfaces.SpotifyUserService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SpotifyUserServiceImpl implements SpotifyUserService {
    private final RestTemplate restTemplate;

    public SpotifyUserServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final String userUrl = "https://api.spotify.com/v1/me";

    @Override
    public SpotifyUserNameAndEmailModel getSpotifyUserData(String token) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(userUrl, HttpMethod.GET, httpEntity, String.class);
        SpotifyUserNameAndEmailModel spotifyUserNameAndEmailModel = objectMapper.readValue(responseEntity.getBody(), SpotifyUserNameAndEmailModel.class);
//        System.out.println(spotifyUserNameAndEmailModel.getEmail());
        return spotifyUserNameAndEmailModel;
    }

}
