package com.spotifydatingapp.SpotifyDatingApp.services.impls;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spotifydatingapp.SpotifyDatingApp.models.SpotifyUserNameAndEmailModel;
import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.AddUserDetailsFromSpotify;
import com.spotifydatingapp.SpotifyDatingApp.repositories.interfaces.SearchIfUserExists;
import com.spotifydatingapp.SpotifyDatingApp.services.interfaces.SpotifyJwtCommonService;
import com.spotifydatingapp.SpotifyDatingApp.services.interfaces.SpotifyUserService;
import com.spotifydatingapp.SpotifyDatingApp.utils.JwtUtil;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class SpotifyJwtCommonServiceImpl implements SpotifyJwtCommonService {

    private final SpotifyUserService spotifyUserService;
    private final SearchIfUserExists searchIfUserExists;
    private final JwtUtil jwtUtil;
    private final AddUserDetailsFromSpotify addUserDetailsFromSpotify;

    public SpotifyJwtCommonServiceImpl(SpotifyUserService spotifyUserService, SearchIfUserExists searchIfUserExists, JwtUtil jwtUtil, AddUserDetailsFromSpotify addUserDetailsFromSpotify) {
        this.spotifyUserService = spotifyUserService;
        this.searchIfUserExists = searchIfUserExists;
        this.jwtUtil = jwtUtil;
        this.addUserDetailsFromSpotify = addUserDetailsFromSpotify;
    }

    @Override
    public JSONObject getJwtAndResult(String token) throws JsonProcessingException {
        SpotifyUserNameAndEmailModel spotifyUserNameAndEmailModel = spotifyUserService.getSpotifyUserData(token);
        Boolean ifExists = searchIfUserExists.checkUserExistenceInRepository(spotifyUserNameAndEmailModel);
        if (!ifExists){
            addUserDetailsFromSpotify.addBasicUserInfo(spotifyUserNameAndEmailModel);
        }
        String jwtToken = jwtUtil.generateToken(spotifyUserNameAndEmailModel.getEmail());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("jwt", jwtToken);
        jsonObject.put("userExists", ifExists);
        jsonObject.put("email", spotifyUserNameAndEmailModel.getEmail());
        jsonObject.put("display_name", spotifyUserNameAndEmailModel.getDisplay_name());
        return jsonObject;
    }
}
